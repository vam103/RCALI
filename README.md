# RCALI

[![pipeline status](https://gitlab.paca.inra.fr/biosp/RCALI/badges/master/pipeline.svg)](https://gitlab.paca.inra.fr/biosp/RCALI/commits/master)


> porting RCALI to new R versions

More informations :

* [RCALI](http://genome.jouy.inra.fr/logiciels/RCALI/)
* [Califloop](http://genome.jouy.inra.fr/logiciels/califlopp/welcome.html)


